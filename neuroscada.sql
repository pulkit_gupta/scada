-- phpMyAdmin SQL Dump
-- version 3.3.9
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Dec 02, 2012 at 09:23 AM
-- Server version: 5.5.8
-- PHP Version: 5.3.5

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `scada`
--

-- --------------------------------------------------------

--
-- Table structure for table `contacts`
--

CREATE TABLE IF NOT EXISTS `contacts` (
  `name` text NOT NULL,
  `message` text NOT NULL,
  `email` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `contacts`
--

INSERT INTO `contacts` (`name`, `message`, `email`) VALUES
('hh', 'hh', 'hh'),
('Ankur Sethi', 'hello! How are you?', 'get.me .ankur@gmail.com'),
('', '', ''),
('', '', ''),
('', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `tickets`
--

CREATE TABLE IF NOT EXISTS `tickets` (
  `name` text NOT NULL,
  `email` text NOT NULL,
  `phone` text NOT NULL,
  `subject` text NOT NULL,
  `message` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tickets`
--

INSERT INTO `tickets` (`name`, `email`, `phone`, `subject`, `message`) VALUES
('Pulkit Gupta', 'pulkit.itp@gmail.com', '919958140688', 'Problem with login', 'Actually sir, i a,m ion problem with the login pasrty of the website/.'),
('Pulkit Gupta', 'pulkit.itp@gmail.com', '919958140688', 'Problem with login', 'Hello Hello'),
('Pulkit Gupta', 'pulkit.itp@gmail.com', '919958140688', 'testt', 'hello');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `name` text NOT NULL,
  `email` varchar(200) NOT NULL,
  `password` text NOT NULL,
  `mobile` text NOT NULL,
  `passwordreset` text NOT NULL,
  `timestamp` text NOT NULL,
  `active` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`name`, `email`, `password`, `mobile`, `passwordreset`, `timestamp`, `active`) VALUES
('pulkit_gupta', 'pulkit.itp@gmail.com', 'd1d5ab287fa5f4d3f7937a777c021c5c', '9958140688', '219c00481c2150a09a750eaecc15ac6c', '1354454383550', 1),
('pulkit_gupta', 'p@p.com', 'f3018c331dd28c32473b5b12f3c41a5f', 'pp', '1b93a10a12e060b8146b820e4e279cdb', '1353348961979', 1),
('pulkit_gupta', 'p@pk.com', 'f0f182d47469486a7e8a81078a0d230b', '', 'dc157d67ffa71f4edabbbe9d8897550a', '1353350057192', 1),
('pulkit_gupta', 'p@ll.com', 'e09638ae37d3e1a437cad307f3845f35', '', 'b9380d67b6149211c428c111562833ff', '1353350138336', 1);
