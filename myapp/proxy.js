var httpProxy = require('http-proxy') ; 

var options = {
  hostnameOnly: true,
  router: {
    'localhost': '127.0.0.1:3000',
    'pulkit-laptop': '127.0.0.1:1000'
  }
}
var proxyServer = httpProxy.createServer(options);
proxyServer.listen(80,function(){
	console.log("proxy server running on port 80 which is serving child servers");
});