
/**
 * Module dependencies.
 */

var express = require('express')
  , routes = require('./routes')
  , user = require('./routes/user')
  , member = require('./routes/member')
  , http = require('http')
  , path = require('path')
  , config = require('./config').Config
  , fs =require('fs');

var app = express();
var server = http.createServer(app);


var RedisStore = require ( 'connect-redis' ) ( express );
var sessionStore = new RedisStore ();
var Memstore = express.session.MemoryStore;

var logFile = fs.createWriteStream('./scada.log',{flags: 'a'});
app.configure(function(){  
  app.set('views', __dirname + '/views');
  app.set('view engine', 'ejs');
  //app.use(express.favicon());  
  app.use(express.logger({stream: logFile}));
  app.use(express.bodyParser());
  app.use(express.methodOverride());  
  app.use(require('stylus').middleware(__dirname + '/public'));
  
  app.use(express.cookieParser()); 
  app.use(express.session({ secret: 'somerandomstring', store: Memstore(    ) }));  
  /*app.use ( express.session ( {
      secret: "keyboardat", store: sessionStore, key: 'hello.sid'
   } ) );*/
   app.use(app.router);
   app.use(express.static(path.join(__dirname, 'public')));
});


app.configure('development', function(){
  app.use(express.errorHandler());
});


function requiresLogin(req, res, next){
  if(req.session.user){
  console.log("session is there");
  next();
  }
  else{
  console.log("session is not there");
  res.redirect('/login?redir='+req.url);
  }
}
function loginNotAllowed(req, res, next){
  if(req.session.user){
  console.log("session is there");
  res.redirect('member');
  }
  else{
  console.log("session is not there");
  next();
  }
}


app.get('/', routes.index);
app.get('/register', loginNotAllowed, user.register);
app.post('/adduser', user.adduser);
app.get('/login', loginNotAllowed, user.login);
app.post('/loginuser', user.loginuser); 
app.post('/forgot', user.forgot); 
app.get('/resetpassword', user.resetpassword); 
app.post('/newpassword', user.newpassword); 
app.get('/logout', requiresLogin, user.logout);
app.get('/about', routes.about);
app.get('/contact', routes.contact);
app.post('/sendmessage', routes.sendmessage);
app.get('/member',requiresLogin, member.play);
app.get('/help',routes.help);
app.get('/ticket',routes.ticket);
app.post('/ticketsend',routes.ticketsend);


server.listen(config.socketPort, function(){
  console.log("Express server listening on port " + config.socketPort);
});
