
var config = require('../config').Config;
var postmark = require("postmark")(config.mailKey);
var mysql = require('mysql');

var connection = mysql.createConnection({
  	host     : 'localhost',
  	user     : 'root',
    	password : config.dbPassword,
    	database : config.dbName
});

function handleDisconnect(connection) {
  connection.on('error', function(err) {
    if (!err.fatal) {
      return;
    }

    if (err.code !== 'PROTOCOL_CONNECTION_LOST') {
      throw err;
    }

    console.log('Re-connecting lost connection: ' + err.stack);

    connection = mysql.createConnection({
    host     : 'localhost',
    user     : 'root',
      password : config.dbPassword,
      database : config.dbName
  });


    handleDisconnect(connection);
    connection.connect();
  });
}

handleDisconnect(connection);
var hash = require('node_hash');

exports.register = function(req, res){
	res.render('register',{locals: {title: "Register", login: req.session.user}});
}

exports.adduser = function(req, res){

	var fname = req.body.name;
	var femail = req.body.email;
	var fpassword = req.body.password;
	var fmobile = req.body.tel;	
	var fpasswordrest = hash.md5(String(femail));	//passwordrest			
	var date = new Date();
	var ftimestamp = date.getTime();	
	fpassword = hash.md5(String(fpassword), String(ftimestamp));	

	var sql    = 'SELECT * FROM users WHERE email='+connection.escape(femail);;
	connection.query(sql, function(err, results) {
		if(results.length==1)
		{
		
				res.render('register', {locals:{ title:"Register", login: req.session.user, msg: 'This email ID is already registered with this us', redir: "/member"}});
		}
		else	
		{				
			connection.query('INSERT INTO users SET ?', {name: fname , email: femail, password: fpassword, mobile: fmobile, passwordreset: fpasswordrest, timestamp: ftimestamp, active: "1" }, function(err, result) 
			{
				if(err){
					console.log(err);
				}					
				else
				{				
					/*postmark.send({
				   	"From": config.siteEmail, 
				   	"To": str9,
				   	"Subject": "Acount activation link", 
				   	"HtmlBody": "Dear User, <br><br>Please click here to activate your account http://"+config.appURL+"/activate?q="+str12+" If link not avaliable, Copy it in other tab of your web browser to activate your account.<br><br><br>regards, <br>Team E-Fest"
					}, function (err, to) {
					    if (err) {
				        console.log(err);
				        return;				   	}
				   		console.log("Email sent to: %s", to);
					});
					*/																			
					req.session.user = {name: fname, email: femail};
					req.session.cookie.expires = false;
					console.log("session variable created for "+femail);
					//res.render('login', {locals: { title: "Login", login: "true",msg: 'Email Confirmation will be sent to your email ID with in next 5 Minutes. Please click that link to verify your email id.',redir: "/member" }});
					res.redirect('member');
				}		
			});
		} //else
	});//sql
};


exports.login = function(req, res){
	res.render('login', {locals: {title: "Login", login: req.session.user, redir: req.query.redir}});
};


exports.loginuser = function(req, res){
	console.log(req.body.email+req.body.password+"status:"+req.body.checkbox);
	var sql= 'SELECT * FROM users WHERE active =1 AND email = ' + connection.escape(req.body.email);	
		connection.query(sql, function(err, results) {				
			if(err)
			{
				console.log(err);
			}
			else if(results[0]==undefined)
			{			
				res.render('login', {locals:{ title: "Login", login: req.session.user, msg: 'EMail ID entered is not registered or activated.', redir: req.body.redir}});	
			}
			
			else
			{				
				var salt = results[0].timestamp;
				var email= results[0].email;			
				var sql= 'SELECT name, email FROM users WHERE email = '+connection.escape(email)+'AND password = ' + connection.escape(hash.md5(String(req.body.password),String(salt)));
				connection.query(sql, function(err, results) {	
					if(err)
					{
						console.log(err);
					}
					else if(results[0]==undefined)
					{
						console.log("invalid password");
						res.render('login', {locals:{ title: "Login", login: req.session.user, msg: 'Invalid Password!', redir: req.body.redir}});
					}
					else
					{	
						req.session.user = results[0];																
						var redir = "/member";
						if(req.body.redir != "undefined"){
							redir = req.body.redir;
					}
						res.redirect(redir);
					}
				});
			}		
	});
};

exports.logout = function(req, res){
	console.log(req.session.user.name +"logging out");
	req.session.user = undefined;
	delete req.session.user;	
	res.redirect('/');
};


exports.forgot = function(req, res){

	var email1 = req.body.email;	
	var sql    = 'SELECT * FROM users WHERE email='+connection.escape(email1);
	connection.query(sql, function(err, results) {
		if(err)
		console.log(err);		
		
		if(results[0]!=undefined)
		{							
			var resethash = results[0].passwordreset;
			var sql= 'UPDATE users SET active=0 WHERE email='+connection.escape(email1);
			connection.query(sql, function(err, results) {	
				if (err) 
				{//throw err;
				}
			});
			
			
			postmark.send({
			    "From": config.siteEmail, 
			    "To": email1, 
			    "Subject": "Password reset-link", 
			    "HtmlBody": "Dear User,<br><br> Please click here to reset your account password http://"+config.appURL+"/resetpassword?q="+resethash+"   or paste it in URL<br><br><br>regards, <br> Team-Efest"
				}, function (err, to) {
					    if (err) {
					        console.log(err);
					        return;
			 	   }
			 	   console.log("Email sent to: %s", to);
			});
			
			
			res.render('login', {locals:{ title: "Login", login: req.session.user, msg: 'Reset password link sent to your registered Email Id', redir: req.body.redir}});			
		}
		else
		{
			res.render('login', {locals:{ title: "Login", login: req.session.user, msg: 'Email is not registered with us, register it!', redir: req.body.redir}});						
		}
	});//sql		
	
};

exports.resetpassword = function(req, res){
	
	var sql= 'SELECT * FROM users WHERE active = 0 AND passwordreset = ' + connection.escape(req.query.q);
	connection.query(sql, function(err, results) {	
			
		if(results[0]==undefined)
		{			
			res.render('login', {locals:{ title: "Login", login: req.session.user, msg: 'Link has already expired', redir: req.body.redir}});						
		}
		
		else
		{
			res.render('resetpassword', {locals:{title: "Reset Password", resetlink: req.query.q}});	
		}		
	});

};

exports.newpassword = function(req, res){

	
	var str13 = req.body.password;
	var date = new Date();
	var str14 = date.getTime()-date.getTimezoneOffset() * 60*1000;  //timestamp
	var salt=String(str14);
        str13 = hash.md5(str13, salt);	               
	var sql= "UPDATE users SET active=1, password="+connection.escape(str13)+", timestamp="+connection.escape(str14)+" WHERE active = 0 AND passwordreset ="+connection.escape(req.body.resetlink);
	
	connection.query(sql, function(err, results) {	
	
		res.render('login', {locals:{ title: "Login", login: req.session.user, msg: 'Your password is changed successfully. Please Login', redir: req.body.redir}});
	});

};