
/*
 * GET home page.
 */
var config = require('../config').Config;
var mysql = require('mysql');

var connection = mysql.createConnection({
  	host     : 'localhost',
  	user     : 'root',
    	password : config.dbPassword,
    	database : config.dbName
});
function handleDisconnect(connection) {
  connection.on('error', function(err) {
    if (!err.fatal) {
      return;
    }

    if (err.code !== 'PROTOCOL_CONNECTION_LOST') {
      throw err;
    }

    console.log('Re-connecting lost connection: ' + err.stack);

    connection = mysql.createConnection({
    host     : 'localhost',
    user     : 'root',
      password : config.dbPassword,
      database : config.dbName
  });


    handleDisconnect(connection);
    connection.connect();
  });
}

handleDisconnect(connection);

exports.index = function(req, res){
  res.render('index', { locals: {title: "Home", login: req.session.user} });
};

exports.about = function(req, res){
	res.render('about', {locals:{title: "About", login: req.session.user}})
};

exports.contact = function(req, res){	
	res.render('contact', {locals: {title: "Contact", login: req.session.user}})
};

exports.sendmessage = function(req, res){
	var femail = req.body.email, fname= req.body.name, fmessage= req.body.message;
	connection.query('INSERT INTO contacts SET ?', {name: fname , email: femail, message: fmessage}, function(err, result) 
	{
		if(err){
			console.log(err);
		}
		else
		{
			console.log("contact data inserted");
		}
	});
	res.render('contact', {locals: {title: "Contact", login: req.session.user, msg: "You message is sent"}})		
};

exports.help = function(req, res){
	res.render('help', {locals:{title: "Help", login: req.session.user}})
};

exports.ticket = function(req, res){	
	res.render('ticket', {locals:{title: "Ticket", login: req.session.user}})
};

exports.ticketsend = function(req, res){
	var femail = req.body.email, fname= req.body.name, fmessage= req.body.message, fphone= req.body.phone, fsubject=req.body.subject;
	connection.query('INSERT INTO tickets SET ?', {name: fname , email: femail, phone: fphone, subject: fsubject, message: fmessage}, function(err, result) 
	{
		if(err){
			console.log(err);
		}
		else
		{
			console.log("Ticket data inserted");
		}
	});
	res.render('ticket', {locals:{title: "Ticket", login: req.session.user, msg: "Your message has been sent. You will be responded shortly."}});
};

