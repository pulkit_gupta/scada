function setSpeedMeter(gauge)
{
	gauge.Set('chart.angles.start', PI - (PI / 8));
	gauge.Set('chart.angles.end', TWOPI + (PI / 8));
	gauge.Set('chart.text.color', 'white');
	gauge.Set('chart.tickmarks.big.color', 'white');
	gauge.Set('chart.tickmarks.medium.color', 'white');
	gauge.Set('chart.tickmarks.small.color', 'white');
	gauge.Set('chart.border.outer', 'transparent');
	gauge.Set('chart.border.inner', 'transparent');
	gauge.Set('chart.colors.ranges', []);
	gauge.Set('chart.background.color', 'transparent');
	gauge.Set('chart.border.outline', 'transparent');
	gauge.Set('chart.needle.colors', ['red']);
	gauge.Set('chart.needle.type', 'line');
	gauge.Set('chart.needle.tail', true);
	gauge.Set('chart.needle.size', 55);
	gauge.Set('chart.centerpin.radius', 0.1);
	gauge.Set('chart.title.top', 'Speed');
	gauge.Set('chart.title.top.color', 'white');
	gauge.Set('chart.labels.centered', true);
	gauge.Set('chart.labels.offset', 7);
	gauge.Set('chart.background.color', 'black');
	gauge.Set('chart.border.outer', '#666');
	gauge.Set('chart.border.inner', '#333');
	gauge.Set('chart.background.gradient', true);      
}
