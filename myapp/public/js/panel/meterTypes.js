function setSpeedMeter(gauge)
{
	gauge.Set('chart.angles.start', PI - (PI / 8));
	gauge.Set('chart.angles.end', TWOPI + (PI / 8));
	gauge.Set('chart.text.color', 'white');
	gauge.Set('chart.tickmarks.big.color', 'white');
	gauge.Set('chart.tickmarks.medium.color', 'white');
	gauge.Set('chart.tickmarks.small.color', 'white');
	gauge.Set('chart.border.outer', 'transparent');
	gauge.Set('chart.border.inner', 'transparent');
	gauge.Set('chart.colors.ranges', []);
	gauge.Set('chart.background.color', 'transparent');
	gauge.Set('chart.border.outline', 'transparent');
	gauge.Set('chart.needle.colors', ['red']);
	gauge.Set('chart.needle.type', 'line');
	gauge.Set('chart.needle.tail', true);
	gauge.Set('chart.needle.size', 55);
	gauge.Set('chart.centerpin.radius', 0.1);
	gauge.Set('chart.title.top', 'RPM');
	gauge.Set('chart.title.bottom', 'x1000');
	gauge.Set('chart.title.top.color', 'white');
	gauge.Set('chart.labels.centered', true);
	gauge.Set('chart.labels.offset', 7);
	gauge.Set('chart.background.color', 'black');
	gauge.Set('chart.border.outer', '#666');
	gauge.Set('chart.border.inner', '#333');
	gauge.Set('chart.background.gradient', true);      
	gauge.ondraw = function myCenterpin (obj)
	{
	    // This circle becomes the border of the center
	    obj.context.beginPath();
	    obj.context.fillStyle = '#aaa';
	    obj.context.arc(obj.centerx, obj.centery, 10, 0, TWOPI, false);
	    obj.context.fill();
	}

	gauge.Draw();
	var tempval = 1;
	$("#rpm").val(tempval).change(function () {        
	    var v = $(this).val();
	    if (v && !isNaN(+v)) {
	        tempval = +v;
	        if (tempval < 0)
	            tempval = 0;
	        if (tempval > 10)
	            tempval = 10;
	        $(this).val("" + tempval);
	        gauge.value =tempval; 
	        RGraph.Clear(gauge.canvas);
	        RGraph.Effects.Gauge.Grow(gauge);
	    }
	});

	           /**
	           * This facilitates being able to click the chart to adjust it (animated)
	           */
	/*gauge.canvas.onclick_rgraph = function (e)
    {
    	var obj   = e.target.__object__;
    	var value = obj.getValue(e);
    	obj.value = value;
    	RGraph.Effects.Gauge.Grow(obj);
    }*/
}

