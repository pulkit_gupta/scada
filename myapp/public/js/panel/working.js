var voltmeter = new jGauge(), ammeter = new jGauge(), power = new jGauge(); // Create a new jGauge.
var thermometer1 = new RGraph.Thermometer('thermo1', 0,100,0.2), thermometer2 = new RGraph.Thermometer('thermo2', 0,100,0.2);
var waterLevel = new RGraph.VProgress('waterlevel', 79,100);
var oxygen = new RGraph.Gauge('ox', 0, 100, 84);
var speed = new RGraph.Gauge('speed', 0, 10, 1);



/**********RGraph function***********************************************/

function setProgress(prog)
{
    prog.Set('chart.color', 'red');
    prog.Set('chart.text.color','#3399CC');
    prog.Set('chart.title',"Fluid level");
    prog.Set('chart.title.color','#3399CC');
    prog.Set('chart.margin', 5);
    prog.Set('chart.tickmarks.inner', true);
    prog.Set('chart.label.inner', true);
    //prog.Set('chart.units.post', '%');
    
    prog.Draw();
}

function setGauge(gauge, titletop, titledown)
{    
    gauge.Set('chart.scale.decimals', 0);
    gauge.Set('chart.tickmarks.small', 50);
    gauge.Set('chart.tickmarks.big',5);
    gauge.Set('chart.title.top', titletop);
    gauge.Set('chart.title.top.size', 20);
    gauge.Set('chart.title.bottom', titledown);
    gauge.Set('chart.title.bottom.color', '#aaa');
    RGraph.Effects.Gauge.Grow(gauge);
    
    if (!RGraph.isOld()) {
        gauge.canvas.onmousedown = function (e)
        {
            var obj   = e.target.__object__;
            var value = obj.getValue(e);
            
            obj.value = value;
            RGraph.Effects.Gauge.Grow(obj);
        }
    }
}
function setThermo(thermometer, color, text, inputonly, field)
{
    var grad = thermometer.context.createLinearGradient(15,0,85,0);
    grad.addColorStop(0,color);    
    if(color=='blue')
     grad.addColorStop(0.5,'#99f');
    else
      grad.addColorStop(0.5,'#9f9');

    grad.addColorStop(1,color);
    thermometer.Set('chart.text.color', '#3399CC');     
    thermometer.Set('chart.colors', [grad]);
    thermometer.Set('chart.title.side', text);     //3399CC    
    thermometer.Set('chart.scale.visible', true);
    thermometer.Set('chart.scale.decimals', 2);
    thermometer.Set('chart.gutter.right', 25);
    thermometer.value =30;
    thermometer.Draw();

    if(!inputonly)
    {
        thermometer.canvas.onmousedown = function (e)
        {                    
            var obj   = e.target.__object__;
            var value = obj.getValue(e);

            obj.value = value;            
            RGraph.Effects.Thermometer.Grow(obj);
            $("#"+field).val("" + value);
        }
        var tempval = 30;

        $("#"+field).val(tempval).change(function () {        
            var v = $(this).val();
            if (v && !isNaN(+v)) {
                tempval = +v;
                if (tempval < 0)
                    tempval = 0;
                if (tempval > 100)
                    tempval = 100;
                $(this).val("" + tempval);
                thermometer.value =tempval; RGraph.Clear(thermometer.canvas);RGraph.Effects.Thermometer.Grow(thermometer);
            }
        });
    }


}


/************************************************************************/




/*********************Gauge library*************************************/
function createMeter(domid, meter,start, end, count, suffix){    
    meter.id = domid; // Link the new jGauge to the placeholder DIV.
    meter.autoPrefix = ''; // Use SI prefixing (i.e. 1k = 1000).
    meter.imagePath = 'images/panel/jgauge_face_taco.png';
    meter.segmentStart = -225
    meter.segmentEnd = 45
    meter.width = 170;
    meter.height = 170;
    meter.needle.imagePath = 'images/panel/jgauge_needle_taco.png';
    meter.needle.xOffset = 0;
    meter.needle.yOffset = 0;
    meter.label.yOffset = 55;
    meter.label.color = '#fff';
    meter.label.precision = 2; // 0 decimals (whole numbers).
    meter.label.suffix = suffix; // Make the value label
    meter.ticks.labelRadius = 45;
    meter.ticks.labelColor = '#0ce';
    meter.ticks.start = start;
    meter.ticks.end = end;
    meter.ticks.count = count;
    meter.ticks.color = 'rgba(0, 0, 0, 0)';
    meter.range.color = 'rgba(0, 0, 0, 0)';
}

function setVal(value)
{
    demoGauge1.setValue(value);
    demoGauge2.setValue(value);
}

function bumpVal(value)
{
    demoGauge1.setValue(demoGauge1.value + value);
    demoGauge2.setValue(demoGauge2.value + value);
}
function setTickCount(value)
{
    demoGauge1.ticks.count = value;
    demoGauge1.updateTicks();
    
    demoGauge2.ticks.count = value;
    demoGauge2.updateTicks();
}

function setRange(radius, thickness, start, end, color)
{
    demoGauge1.range.radius = radius;
    demoGauge1.range.thickness = thickness;
    demoGauge1.range.start = start;
    demoGauge1.range.end = end;
    demoGauge1.range.color = color;
    demoGauge1.updateRange();
    
    demoGauge2.range.radius = radius;
    demoGauge2.range.thickness = thickness;
    demoGauge2.range.start = start;
    demoGauge2.range.end = end;
    demoGauge2.range.color = color;
    demoGauge2.updateRange();
}
function randVal(meter, randpar)
{
    var newValue;
    if (Math.random() > 0.8) // Allow needle to randomly pause.
    {
        newValue = meter.value + (Math.random() * randpar - randpar*7/10);
       if (newValue >= meter.ticks.start && newValue <= meter.ticks.end)
       {
           // newValue is within range, so update.
           meter.setValue(newValue);
       }
   }
}
/**********************************Gauge library*****************************************************************************/

$(document).ready(function()
{    
    setSpeedMeter(speed);
    setProgress(waterLevel);
    setGauge(oxygen, "Oxygen", "pascal");    
    $(realPanel);
    setThermo(thermometer1, 'green', 'Temperature (In Celsius)', false,"tempval");
    setThermo(thermometer2, 'blue', 'Heat Sink Temperature', true);
    createMeter('voltage', voltmeter, 0, 30, 7, 'V');
    createMeter('current', ammeter, 0, 6000, 7, 'mA');
    createMeter('power', power, 0, 200, 7, 'W');
    
    voltmeter.init(); 
    ammeter.init(); power.init();    
    
    voltmeter.setValue(20);
    ammeter.setValue(0);
    power.setValue(10);
    //setInterval('randVal(voltmeter, 100)', 100);
    setInterval('randVal(ammeter, 1000)', 100);
});